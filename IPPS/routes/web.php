<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

//login routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/resetpword', 'RecordsController@resetpword')->name('resetpword');
Route::get('admin/routes', 'HomeController@admin')->middleware('admin');

//user records routes
Route::get('delete/{id}', 'RecordsController@destroy')->middleware('auth')->middleware('admin');
Route::get('edit/{id}', 'RecordsController@edit')->name('edit_user')->middleware('auth')->middleware('admin');
Route::patch('edit_user/{id}', 'RecordsController@update')->middleware('auth')->middleware('admin');
Route::get('/usermgt', 'RecordsController@registernewPage')->name('user_mgt')->middleware('auth')->middleware('admin');
Route::post('/info', 'RecordsController@store')->middleware('auth')->middleware('admin');

Route::get('datatable/getdata', 'RecordsController@getPosts')->name('getdata');

//change password routes
Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::patch('/changePassword','HomeController@changePassword')->name('changePassword');

//assignatory routes
Route::get('/signatory', 'AssignatoryController@index')->name('rbd')->middleware('auth')->middleware('admin');
Route::get('/signatory/aa', 'AssignatoryController@index')->name('aa')->middleware('auth')->middleware('admin');
Route::get('/signatory/cash', 'AssignatoryController@index')->name('cash')->middleware('auth')->middleware('admin');
Route::get('/signatory/approval', 'AssignatoryController@index')->name('pra')->middleware('auth')->middleware('admin');
Route::get('/signatory/technical', 'AssignatoryController@index')->name('twg')->middleware('auth')->middleware('admin');

Route::get('signatory/getdata/r', 'AssignatoryController@getPostsR')->name('signatory/getdata/r');
Route::get('signatory/getdata/a', 'AssignatoryController@getPostsA')->name('signatory/getdata/a');
Route::get('signatory/getdata/c', 'AssignatoryController@getPostsC')->name('signatory/getdata/c');
Route::get('signatory/getdata/aa', 'AssignatoryController@getPostsAA')->name('signatory/getdata/aa');
Route::get('signatory/getdata/twg', 'AssignatoryController@getPostsT')->name('signatory/getdata/twg');


Route::post('/registersignatory', 'AssignatoryController@store')->middleware('auth')->middleware('admin');
Route::get('/signatory/edit/{id}', 'AssignatoryController@edit')->middleware('auth')->middleware('admin');
Route::patch('signatory/update/{id}', 'AssignatoryController@update')->middleware('auth')->middleware('admin');

Route::get('activate/{id}', 'AssignatoryController@ActivateSignatory')->middleware('auth')->middleware('admin');
Route::get('deactivate/{id}', 'AssignatoryController@DeactivateSignatory')->middleware('auth')->middleware('admin');
Route::get('deletesignatory/{id}', 'AssignatoryController@destroy')->middleware('auth')->middleware('admin');

//purchase request routes
Route::get('/pr/data', 'PurchaseRequestController@getPR')->name('pr.data');
Route::get('/pr/form', 'PurchaseRequestController@index')->name('pr.form');

Route::post('/pr/submit', 'PurchaseRequestController@store');
Route::get('/pr/edit/{id}', 'PurchaseRequestController@edit')->name('pr.edit');
Route::patch('/pr/update/{id}', 'PurchaseRequestController@update');
Route::get('/pr/close/{id}', 'PurchaseRequestController@closePR')->name('pr.close');
Route::get('/pr/delete/{id}', 'PurchaseRequestController@destroy')->name('pr.delete');

Route::get('/pr/items/{id}', 'PurchaseRequestItemController@show')->name('pr.items');
Route::post('/pr/items/add', 'PurchaseRequestItemController@store')->name('pr.items.add');

Route::get('/pr/items/update/{id}', 'PurchaseRequestItemController@updateitems')->name('pr.update.items');


//soledistribuitor routes
Route::get('/soledist','SoleDistController@index')->middleware('auth')->middleware('admin');
Route::post('/soledist/register', 'SoleDistController@store')->name('soledistreg')->middleware('auth')->middleware('admin');
Route::get('soledist/delete/{id}', 'SoleDistController@destroy')->middleware('auth')->middleware('admin');





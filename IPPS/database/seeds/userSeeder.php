<?php

use Illuminate\Database\Seeder;
use App\User;


class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        user::create([
					'name' => "admin1",
					'password' => bcrypt('admin1'),
					'department' => "ICT",
					'wholename' => "OJT",
					'isAdmin' => "1",
                    'isBACSec' => "0",


		]);
        user::create([
                    'name' => "user1",
                    'password' => bcrypt('users1'),
                    'department' => "PDO",
                    'wholename' => "USER_ACCT1",
                    'isBACSec' => "1",
                    'isAdmin' => "0",
        ]);
        
    }
}

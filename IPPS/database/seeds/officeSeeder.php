<?php

use Illuminate\Database\Seeder;
use App\Office;

class officeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        office::create([
					'iso_code' => "BFP",
					'office_name' => "Bureau of Fire Protection",	
		]);
		office::create([	
					'iso_code' => "DILG",
					'office_name' => "City Department of Interior and Local Government",
		]);			
		office::create([
					'iso_code' => "EEM",
					'office_name' => "City Market Office",
		]);
		office::create([	
					'iso_code' => "COC",
					'office_name' => "Clerk of Court",]);

		office::create([	
					'iso_code' => "COMELEC",
					'office_name' => "Commission on Elections",]);

		office::create([
					'iso_code' => "COA",
					'office_name' => "Commision on Audit",]);

		office::create([
					'iso_code' => "MTCCB1",
					'office_name' => "Court Branch 1",]);

		office::create([
					'iso_code' => "MTCCB2",
					'office_name' => "Court Branch 2",]);
		office::create([
					'iso_code' => "DEPED",
					'office_name' => "Department of Education",]);
		office::create([
					'iso_code' => "ELEC",
					'office_name' => "Electrical Office",]);
		office::create([
					'iso_code' => "EAS",
					'office_name' => "Engineering and Architectural Services",]);
		office::create([		
					'iso_code' => "HRM",
					'office_name' => "Human Resource Management Office",]);
		office::create([	
					'iso_code' => "IDS",
					'office_name' => "Information Dissemination Section",]);
		office::create([
					'iso_code' => "ICT",
					'office_name' => "Information Technology Section",]);
		office::create([		
					'iso_code' => "LUSCM",
					'office_name' => "La Union Science Centrum and Museum",]);
		office::create([	
					'iso_code' => "LNMB",
					'office_name' => "Liga ng mga Barangay",]);
		office::create([
					'iso_code' => "LEBDO",
					'office_name' => "Local Economic, Business and Dev't Office",]);

		office::create([
					'iso_code' => "OSM",
					'office_name' => "Office for Strategy Management",]);

		office::create([
					'iso_code' => "ACA",
					'office_name' => "Office of the City Accountant",]);

		office::create([	
					'iso_code' => "ADM",
					'office_name' => "Office of the City Administrator",]);

		office::create([
					'iso_code' => "AGR",
					'office_name' => "Office of the City Agriculturist",]);

		office::create([
					'iso_code' => "OCA",
					'office_name' => "Office of the City Assesor",]);
		office::create([
							'iso_code' => "CBO",
							'office_name' => "Office of the City Budget Officer",]);
		office::create([
							'iso_code' => "ENR",
							'office_name' => "Office of the City Environment and Natural Resources Officer",]);
		office::create([
							'iso_code' => "GSO",
							'office_name' => "Office of the City General Sercices Officer",]);
		office::create([
							'iso_code' => "CHO",
							'office_name' => "Office of the City Health Officer",]);
		office::create([
							'iso_code' => "CLO",
							'office_name' => "Office of the City Legal Officer",]);
		office::create([
							'iso_code' => "LIB",
							'office_name' => "Office of the City Library",]);
		office::create([
							'iso_code' => "OCM",
							'office_name' => "Office of the City Mayor",]);
		office::create([
							'iso_code' => "PDO",
							'office_name' => "Office of the City Planning and Development Coordinator",]);
		office::create([
							'iso_code' => "REG",
							'office_name' => "Office of the City Registrar",]);
		office::create([
							'iso_code' => "SWD",
							'office_name' => "Office of the Social Welfare and Development Officer",]);
		office::create([
							'iso_code' => "CTO",
							'office_name' => "Office of the City Treasurer",]);
		office::create([
							'iso_code' => "CVO",
							'office_name' => "Office of the City Veterenarian",]);
		office::create([
							'iso_code' => "OCVM",
							'office_name' => "Office of the City Vice-Mayor",]);
		office::create([
							'iso_code' => "OPS",
							'office_name' => "Office of the Public Safety",]);
		office::create([
							'iso_code' => "OSP",
							'office_name' => "Office of the Sangguniang Panlungsod",]);
		office::create([
							'iso_code' => "OSSP",
							'office_name' => "Office of the Secretary to the Sangguniang Panlungsod",]);
		office::create([
							'iso_code' => "OSCA",
							'office_name' => "Office of the Senior Citizen"]);
		office::create([
							'iso_code' => "PNP",
							'office_name' => "Philippine National Police"]);
		office::create([
							'iso_code' => "PACU",
							'office_name' => "Public Assistance and Complaints Unit"]);



        		
    }
}

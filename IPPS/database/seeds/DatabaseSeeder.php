<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(officeSeeder::class);
        $this->call(userSeeder::class);
        $this->call(assignatorySeeder::class);
        $this->call(PurchaseRequestSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Assignatory;

class assignatorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        assignatory::create([
					'dept' => "ICT",
					'name' => "Germie O. Deang",
					'position' => "Information Technology Officer II",
					'kind' => "R",
					'status' => "1",	
		]);
        assignatory::create([
                    'dept' => "ICT",
                    'name' => "Jay Carlou C. Sabado",
                    'position' => "Information Technology Officer I",
                    'kind' => "R",
                    'status' => "0",    
        ]);
        assignatory::create([
                    'dept' => "OCM",
                    'name' => "Hon. Hermenegildo A. Gualberto",
                    'position' => "City Mayor",
                    'kind' => "A",
                    'status' => "1",    
        ]);
        assignatory::create([
                    'dept' => "OCM",
                    'name' => "Hon. Alfredo Pablo R. Ortega",
                    'position' => "City Vice Mayor",
                    'kind' => "A",
                    'status' => "0",    
        ]);
    }
}

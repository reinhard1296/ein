<?php

use Illuminate\Database\Seeder;
use App\PurchaseRequestModel;

class PurchaseRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PurchaseRequestModel::create([

            'pr_form_no' =>"PR-ICT-2018-01-0000",
            'department' =>"ADM",
            'section' =>"ICT",
            'purpose' => "Sample",
            'requestor_name'=>"Christian Dave Lara",
            'requestor_position'=>"OJT",
            'supplier_type'=>"Canvass",
            'budget_alloc'=>"1521125",     
            'status'=>"Pending",
            'created_by'=>"1",


		]);
    }
}

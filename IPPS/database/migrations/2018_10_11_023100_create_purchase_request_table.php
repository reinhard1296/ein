<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_request', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pr_form_no');
            $table->string('department');
            $table->string('section')->nullable();
            $table->string('purpose');
            $table->string('requestor_name');
            $table->string('requestor_position');
            $table->decimal('budget_alloc',15,2);
            $table->string('supplier_type'); 
            $table->string('supplier_name')->nullable();
            $table->string('supplier_address')->nullable();           
            $table->string('status');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_request');
    }
}

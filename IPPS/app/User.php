<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wholename', 'contact', 'department', 'name', 'password', 'isAdmin', 'isBACSec',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function offices()
    {
        return $this->belongsTo('App\Office');
    }

    public function signatories()
    {
        return $this->hasMany('App\Assignatory');
    }
}

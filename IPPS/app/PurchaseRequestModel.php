<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestModel extends Model
{
    //
    protected $table = 'purchase_request';

     protected $fillable = [
    	'id',
    	'pr_form_no',
    	'department',
    	'section',
    	'purpose',
    	'requestor_name',
    	'requestor_position',
    	'budget_alloc',
    	'supplier_type',
    	'supplier_name',
    	'status',
    	'created_by',	
	];

    public function pritems()
    {
        return $this->hasMany('App\PurchaseRequestItemModel');
    }
    
}

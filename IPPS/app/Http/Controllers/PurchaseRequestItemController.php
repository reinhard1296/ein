<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Office;
use App\User;
use App\Assignatory;
use App\PurchaseRequestModel;
use App\PurchaseRequestItemModel;
use App\Http\Requests\PurchaseRequestItemsRequest;
use Auth;
use DB;
use Session;

class PurchaseRequestItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // public function getItems()
    // {
    //     $pr_details = PurchaseRequestModel::all();

    //     return \DataTables::of(PurchaseRequestItemModel::query()->where('pr_form_number','=',$pr_details->pr_form_no))
    //     ->addColumn('action', function ($pr) {
    //         return '<a href="#" class="btn btn-primary btn-xs">Edit</a><a href="#" class="btn btn-primary btn-xs">Delete</a>';
    //     })->toJson();    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseRequestItemsRequest $request)
    {
        //

        $add_item=new PurchaseRequestItemModel;
        $add_item->item_no = $request->get('pr_item');
        $add_item->pr_form_number = $request->get('pr_number');
        $add_item->pr_qty = $request->get('pr_qty');
        $add_item->pr_unit = $request->get('pr_unit');
        $add_item->pr_description =$request->get('pr_desc') ;
        $add_item->pr_cost_per_unit =$request->get('pr_cpu') ;
        $add_item->pr_estimated_cost =$request->get('pr_cpi') ;
            

        $add_item->save();
        activity('Added Item')
        ->log('Added new item under: '.$request->get('pr_number'));
        Session::flash('flash_message', 'Added to database'); 
        return response()->json(['success'=>'Added to database']);



        

        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
   
        $pr_details = DB::table('purchase_request')->where('id','=', $id)->value('pr_form_no');
        $pr_table = PurchaseRequestItemModel::all()->where('pr_form_number','=',$pr_details);
        return view('pr-items',compact('pr_details','pr_table'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Office;
use App\Http\Requests\RecordDataRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\DB;
use URL;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;

class RecordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function resetpword()
    {
        return view('auth.resetpword');
    }

    public function registernewPage()
    { 
        // return view('registernew');
      
            // The user is logged in...

        $dept = Office::all();

        $result = DB::table('users')
                ->where('isBACSec', '=', 1)
                ->get();
        return view('registernew', compact('dept','result'));  
    }

    public function getPosts()
    {
        $users = User::query();
        return \DataTables::of($users)
        ->addColumn('action', function ($user) {
                return '<a href="' . URL::to('edit/'.$user->id) .'" class="btn btn-warning btn-sm" title="Edit User"><span class="glyphicon glyphicon-edit"></span></a> <a href="' . URL::to('delete/'.$user->id) .'" class="btn btn-danger btn-sm" title="Delete User"><span class="glyphicon glyphicon-remove"></span></a>';
            })      
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function store(RecordDataRequest $request)
    {

        $Record=new User;
        $Record->wholename = $request->get('wholename');
        $Record->name = $request->get('name');
        $Record->contactno = $request->get('contact');
        $Record->department = $request->get('department');
        $Record->password =bcrypt($request->get('password')) ;

        $Record->isBACSec = $request->get('bacs')?? 0;
        $Record->isAdmin = $request->get('userlvl');         

        $Record->save();
        activity('Add User')
        ->log('Added new record: '.$request->get('name').'-'. $request->get('department'));
        return redirect()->back()->with('flash_message','New record added successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //find record of given id
        $edit_form = User::find($id);
        $dept = Office::all();
        $result = DB::table('users')
                  ->where('isBACSec', '=', 1)
                  ->get();

        //show edit form and pass the info to it
        return View('updateuser',compact('edit_form','dept','result'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        //
        $Record = User::find($id);
        $Record->wholename = $request->get('wholename');
        $Record->contactno = $request->get('contact');
        $Record->department = $request->get('department');
        
        $Record->isBACSec = $request->get('bacs')?? 0;
        $Record->isAdmin = $request->get('userlvl');
        

        
         

        $Record->save();
        activity('Update User')->log('Updated Record: '.$request->get('name').'-'. $request->get('department'));
        return redirect()->route('user_mgt')->with('flash_message','Record updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

        //Find Record
        $record=User::find($id);

        //Delete the record
        $record->delete();
      
        //go back
        activity('Delete user')
        ->log(Auth::user()->name.' deleted user record: '.$record->name);

        return redirect()->back()->with('flash_message','User record deleted.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Office;
use App\User;
use App\Assignatory;
use App\PurchaseRequestModel;
use App\Http\Requests\PurchaseRequestFormRequest;
use DB;

class PurchaseRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $user = Auth::User();
        $prcode = sprintf('%02d',$user->id);
        $year = date('Y'); 
        $row = sprintf('%04d', PurchaseRequestModel::where('pr_form_no', 'like', "PR-".$user->department."-".$year."-".$prcode."-"."____")->count());
        $pr_number = "PR-".$user->department."-".$year."-".$prcode."-".$row;

        // $requestor = DB::table('users')->join('assignatory', 'assignatory.dept', '=', 'users.department')
        //     ->where('assignatory.kind','=','R')->where('assignatory.status','=','1')->select('assignatory.name','assignatory.position')->get();

        $requestor = Assignatory::all()->where('dept','=',Auth::User()->department)->where('kind','=','R')->where('status','=',1)->first();



        // dd($requestor);

        return view('pr-section', compact('pr_number','user','requestor'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getPR()
    {

        $authenticate = Auth::User();

        if($authenticate->isBACSec == 1){
            $pr_query = PurchaseRequestModel::query(); 
        }elseif ($authenticate->department == "ICT") {
            $pr_query = PurchaseRequestModel::query()->Where('section', 'ICT');
        }else{
            $pr_query = PurchaseRequestModel::query()->where('department', $authenticate->department)->where('section','');
        }

        return \DataTables::of($pr_query)
        ->addColumn('action', function ($pr) {
            if ($pr->status == "Cancelled") {
                # code...
            }else{

                if (Auth::User()->isBACSec == 1) {
                    # code...

                        return '<a href="' . route("pr.items", $pr->id) . '" id="view_item" class="btn btn-primary btn-xs" title="Items"><span class="glyphicon glyphicon-th-list"></span></a><a href="' . route("pr.edit", $pr->id) . '" id="edit_form" class="btn btn-warning btn-xs" title="Edit Form"><span class="glyphicon glyphicon-edit"></span></a><a href="' . route("pr.close", $pr->id) . '" id="close_pr" class="btn btn-success btn-xs" title="Close Purchase Request"><span class="glyphicon glyphicon glyphicon-ok"></span></a><a href="' . route("pr.delete", $pr->id) . '" id="cancel_pr" class="btn btn-danger btn-xs" title="Cancel Form"><span class="glyphicon glyphicon-remove"></span></a>';

                    
                }else{
                    return '<a href="' . route("pr.items", $pr->id) . '" id="add_item" class="btn btn-primary btn-xs" title="Add Item"><span class="glyphicon glyphicon-th-list"></span></a><a href="' . route("pr.edit", $pr->id) . '" id="edit_form" id="edit_form" class="btn btn-warning btn-xs" title="Edit Form"><span class="glyphicon glyphicon-edit"></span></a><a href="' . route("pr.delete", $pr->id) . '" id="cancel_pr" class="btn btn-danger btn-xs" title="Cancel Form"><span class="glyphicon glyphicon-remove"></span></a>';
                }

            }
                
                
            })      
        ->make(true);
    }

    public function create()
    {
        //
    }

    public function closePR($id)
    {
        //
         //
        $pr_close = PurchaseRequestModel::find($id);
        $pr_close->status = "Closed";
        

        //Cancel the record
        $pr_close->save();

        //go back
        activity('Close Purchase Request')
        ->log(Auth::user()->name.' closed PR #'.$pr_close->pr_form_no);
        return redirect()->back()->with('flash_message','Purchase Request Closed.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseRequestFormRequest $request)
    {
        //
        $Record=new PurchaseRequestModel;
        $Record->created_by =$request->get('pr_created_by');
        $Record->pr_form_no = $request->get('pr_number');
        $Record->department = $request->get('pr_department');
        $Record->section = $request->get('pr_section');
        $Record->purpose = $request->get('pr_purpose');
        $Record->requestor_name =$request->get('pr_requestor_name') ; 
        $Record->requestor_position =$request->get('pr_requestor_position') ;
        $Record->supplier_type =$request->get('pr_supplier_type') ;
        $Record->supplier_name =$request->get('pr_supplier_name') ;
        $Record->supplier_address =$request->get('pr_supplier_add') ;
        $Record->budget_alloc =$request->get('pr_budget_alloc') ;
        $Record->status =$request->get('pr_status') ;      

        $Record->save();
        activity('Created PR Form')
        ->log('Created PR Form '.$request->get('pr_number'));
        return redirect()->back()->with('flash_message','Registered PR Form #'.$request->get('pr_number').'.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit_form = PurchaseRequestModel::find($id);
        return View('pr-edit', compact('edit_form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PurchaseRequestFormRequest $request, $id)
    {
        //
        $pr_update = PurchaseRequestModel::find($id);
        $pr_update->created_by =$request->get('pr_created_by');
        $pr_update->pr_form_no = $request->get('pr_number');
        $pr_update->department = $request->get('pr_department');
        $pr_update->section = $request->get('pr_section');
        $pr_update->purpose = $request->get('pr_purpose');
        $pr_update->requestor_name =$request->get('pr_requestor_name') ; 
        $pr_update->requestor_position =$request->get('pr_requestor_position') ;
        $pr_update->supplier_type =$request->get('pr_supplier_type') ;
        $pr_update->supplier_name =$request->get('pr_supplier_name') ;
        $pr_update->supplier_address =$request->get('pr_supplier_add') ;
        $pr_update->budget_alloc =$request->get('pr_budget_alloc') ;
        $pr_update->status =$request->get('pr_status') ;
        $pr_update->save();

        activity('Updated PR Form')
        ->log('Updated PR Form '.$request->get('pr_number'));
        return redirect()->route('pr.form')->with('flash_message','Updated PR Form #'.$request->get('pr_number').'. add items for form generation');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pr_delete = PurchaseRequestModel::find($id);
        $pr_delete->status = "Cancelled";
        

        //Cancel the record
        $pr_delete->save();

        //go back
        activity('Cancel Purchase Request')
        ->log(Auth::user()->name.' cancelled PR #'.$pr_delete->pr_form_no);
        return redirect()->back()->with('flash_message','Purchase Request Cancelled.');
    }
}

<form class="form-horizontal" method="POST" action="#">
                    {{ csrf_field() }}
                    <div class="row">

                      <div class="col-md-4">

                        <div class="row form-group {{ $errors->has('pr_number') ? ' has-error' : '' }}">
                                <label for="pr_number" class="col-md-5 control-label">Purhcase Request No.</label>
                                <div class="col-md-7">
                                    <input id="pr_number" type="text" class="form-control" name="pr_number" value="{{ $pr_number }}" readonly autofocus>
                                    @if ($errors->has('pr_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="row form-group {{ $errors->has('pr_department') ? ' has-error' : '' }}">
                                <label for="pr_department" class="col-md-4 control-label">Department</label>
                                <div class="col-md-4">
                                    <input id="pr_department" type="text" class="form-control" name="pr_department" @if($user->department == 'ICT') value="ADM" @else value="{{$user->department}}" @endif readonly required autofocus>
                                    @if ($errors->has('pr_department'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_department') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <input id="pr_section" type="text" class="form-control" name="pr_section" placeholder="Section" @if($user->department == 'ICT') value="{{$user->department}}" @else value="" @endif readonly required autofocus>
                                    @if ($errors->has('pr_section'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_section') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="row form-group {{ $errors->has('pr_requestor_name') ? ' has-error' : '' }}">
                                <label for="pr_requestor_name" class="col-md-4 control-label">Requestor</label>
                                <div class="col-md-8">
                                    <input id="pr_requestor_name" type="text" class="form-control" placeholder="Name" name="pr_requestor_name"  value="{{$requestor[0]->name}}" readonly autofocus>
                                    @if ($errors->has('pr_requestor_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_requestor_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                        </div>
                        
                      </div>

                      <div class="col-md-3">
                        <div class="row form-group {{ $errors->has('pr_purpose') ? ' has-error' : '' }}">
                                <label for="pr_purpose" class="col-md-3 control-label">Purpose</label>
                                <div class="col-md-9">
                                    <textarea style="resize: none; height: 88px;" id="pr_purpose" class="form-control" name="pr_purpose" value="{{ old('pr_purpose') }}" autofocus></textarea>
                                    @if ($errors->has('pr_purpose'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_purpose') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="row form-group {{ $errors->has('pr_requestor_position') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    <input id="pr_requestor_position" type="text" class="form-control" placeholder="Position" name="pr_requestor_position" value="{{$requestor[0]->position}}" readonly autofocus>
                                    @if ($errors->has('pr_requestor_position'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_requestor_position') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                        </div>
                            
                      </div>

                      <div class="col-md-5">
                        <div class="row form-group {{ $errors->has('pr_supplier_type') ? ' has-error' : '' }}">
                                <label for="pr_supplier_type" class="col-md-4 control-label">Supplier Type</label>
                                <div class="col-md-7">
                                    <select id="disttype" class="form-control" name="pr_supplier_type" required>
                                        <option value="1">Canvass</option>
                                        <option value="2">Government Agency</option>
                                        <option value="3">Sole Distributor</option>
                                    </select>
                                    @if ($errors->has('pr_supplier_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_supplier_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="dist row form-group {{ $errors->has('pr_supplier_name') ? ' has-error' : '' }} {{ $errors->has('pr_supplier_add') ? ' has-error' : '' }}" hidden>
                                <label for="pr_supplier_name" id="label-comp" class="col-md-2 control-label">Company</label>
                                <div class="col-md-4">
                                    <input id="dist-name" type="text" class="form-control" placeholder="Name" name="pr_supplier_name" value="{{ old('pr_supplier_name') }}" autofocus>
                                    @if ($errors->has('pr_supplier_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_supplier_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col-md-5">
                                    <input id="dist-add" type="text" class="form-control" name="pr_supplier_add" placeholder="Address" value="{{ old('pr_supplier_add') }}" autofocus>
                                    @if ($errors->has('pr_supplier_add'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_supplier_add') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="row form-group {{ $errors->has('pr_budget_alloc') ? ' has-error' : '' }}">
                                <label for="pr_budget_alloc" class="col-md-4 control-label">Allocated Budget</label>
                                <div class="col-md-7">
                                    <div class="input-group">
                                    <input id="pr_budget_alloc" type="text" class="form-control" name="budget" value="" autofocus>
                                    <span class="input-group-addon">Pesos</span>
                                    </div>
                                    <h5><strong>*Attatch PPMP as proof</strong></h5>
                                    @if ($errors->has('pr_budget_alloc'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pr_budget_alloc') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                        </div>
                            
                      </div>

                    </div> 

                  </form>




<script type="text/javascript">
    //PR Supplier
    $(document).ready(function(){
        $('#disttype').change(
            function () {
                var method = $('option:selected', this).text();
                if (method == "Government Agency") {
                    $('#label-comp').text("Agency");
                    $("#dist-add").prop('readonly', false);
                    $(".dist").show();
                    
                    
                }else if (method == "Sole Distributor") {
                    $(".dist").show();
                    $('#label-comp').text("Company");
                    $("#dist-add").prop('readonly', true);

                }else {
                    $(".dist").hide();
                }
            });
    });
</script>
@extends('layouts.app')
@section('content')

<div class="container">
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Additem</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="pr-number">PR Number: {{$pr_details}}</label>
    </div>
  </div>

  <!-- Table -->
  <table class="table table-bordered table-condensed">
  <thead>
    <tr>
      <th class="col-xs-1">Item #</th>
      <th class="col-xs-1">Qty.</th>
      <th class="col-xs-1">Unit</th>
      <th class="col-xs-4">Description</th>
      <th class="col-xs-2">Cost per Unit</th>
      <th class="col-xs-2">Cost per Item</th>
      <th class="col-xs-2">Action</th>
    </tr>
    <tr>
    <form id="post-form" class="form-inline">
    {{ csrf_field() }}
    <input type="hidden" name="pr_number" value="{{$pr_details}}">
      <th class="col-xs-1">
        <input id="pr_item" class="table-form form-control input-sm" type="text" name="pr_item" readonly>
      </th>
      <th class="col-xs-1">
        <input id="pr_qty" class="table-form form-control input-sm" type="text" name="pr_qty">
      </th>
      <th class="col-xs-1">
        <input id="pr_unit" class="table-form form-control input-sm" type="text" name="pr_unit">
      </th>
      <th class="col-xs-4">
        <input id="pr_desc" class="table-form form-control input-sm" type="text" name="pr_desc">
      </th>
      <th class="col-xs-2">
        <input id="pr_cpu" class="table-form form-control input-sm" type="text" name="pr_cpu">
      </th>
      <th class="col-xs-2">
        <input id="pr_cpi" class="pr-cpi table-form form-control input-sm" type="text" name="pr_cpi" readonly>
      </th>
      <th class="col-xs-2"><button id="submit-btn" class="btn btn-info btn-sm" type="button"><span class="glyphicon glyphicon-plus"></span></button></th>
    </tr>
  </form>
  </thead>
  
  <tbody id="myTable">
    <form class="form-inline" action="#" method="POST">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}

      @foreach($pr_table as $key => $value)
      <tr>
        <td class="col-xs-1">
          <input id="pr_item{{$value->item_no}}" class="table-form form-control input-sm" type="text" name="pr_item" value="{{$value->item_no}}" readonly>
        </td>
        <td class="col-xs-1">
          <input id="pr_qty{{$value->item_no}}" class="table-form form-control input-sm" type="text" name="pr_qty" value="{{$value->pr_qty}}" >
        </td>
        <td class="col-xs-1">
          <input id="pr_unit{{$value->item_no}}" class="table-form form-control input-sm" type="text" name="pr_unit" value="{{$value->pr_unit}}">
        </td>
        <td class="col-xs-4">
          <input id="pr_desc{{$value->item_no}}" class="table-form form-control input-sm" type="text" name="pr_desc" value="{{$value->pr_description}}">
        </td>
        <td class="col-xs-2">
          <input id="pr_cpu{{$value->item_no}}" class="table-form form-control input-sm" type="text" name="pr_cpu" value="{{$value->pr_cost_per_unit}}">
        </td>
        <td class="col-xs-2">
          <input id="pr_cpi{{$value->item_no}}" class="pr-cpi table-form form-control input-sm" type="text" name="pr_cpi" value="{{$value->pr_estimated_cost}}" readonly>
        </td>
        <td class="col-xs-2"><a class="btn btn-warning btn-sm" href="#"><span class="glyphicon glyphicon-pencil"></span></a> <a class="btn btn-danger btn-sm" href="#"><span class="glyphicon glyphicon-remove"></a></td>
      </tr>
      @endforeach

    </form >
  </tbody>
  <tfoot>
    <tr>
      <th colspan="5" id="gt-label">GRAND TOTAL</th>
      <td colspan="2">
        <input id="grand_total" class="table-form form-control input-sm" type="text" name="grand_total" readonly>
      </td>
    </tr>
  </tfoot>
  </table>
    
</div>
</div>



@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){
total();
  
  
  $('#gt-label').css( "text-align", "right" );
  $("tfoot tr").addClass( 'info' );

  var rowCount = $('#myTable tr').length  + 1;
  var rowid = rowCount;

  $('#pr_item').attr('id','pr_item'+rowCount);
  $('#pr_item'+rowCount).val(rowid);
  $('#pr_qty').attr('id','pr_qty'+rowCount);
  $('#pr_unit').attr('id','pr_unit'+rowCount);
  $('#pr_desc').attr('id','pr_desc'+rowCount);
  $('#pr_cpu').attr('id','pr_cpu'+rowCount);
  $('#pr_cpi').attr('id','pr_cpi'+rowCount);

  $('#pr_qty'+rowCount).on('input', function() {
    multiply(rowCount);
  });
  $('#pr_cpu'+rowCount).on('input', function() {
    multiply(rowCount);
  });

  $.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

  $( "#submit-btn" ).click(function(e) {
    e.preventDefault();

  var pr_number = $("input[name=pr_number]").val();
  var pr_item = $("input[name=pr_item]").val();
  var pr_qty = $("input[name=pr_qty]").val();
  var pr_unit = $("input[name=pr_unit]").val();
  var pr_desc = $("input[name=pr_desc]").val();
  var pr_cpu = $("input[name=pr_cpu]").val();
  var pr_cpi = $("input[name=pr_cpi]").val();


     $.ajax({

           type:'POST',

           url:"{{route('pr.items.add')}}",

           data:{
                pr_number:pr_number,
                pr_item:pr_item,
                pr_qty:pr_qty,
                pr_unit:pr_unit,
                pr_desc:pr_desc,
                pr_cpu:pr_cpu,
                pr_cpi:pr_cpi


           },
           success:function(data){

              location.reload();

           }

      });
      
  });
  

});


</script>

<script type="text/javascript">
function total()
  {
    var items = $("#myTable tr .pr-cpi");
    var itemCount = items.length;
    var total = 0;
      for(var i = 0; i < itemCount; i++)
        {
          total = total +  parseFloat(items[i].value);
        }
          $('#grand_total').val(total);
  }  
function multiply(id)
  {
    var total1=parseFloat($('#pr_qty'+id).val())*parseFloat($('#pr_cpu'+id).val());
    $("input[id=pr_cpi" + (id) + "]").val(total1);
    grandTotal();
  }
function grandTotal()
  {
    var items = $(".pr-cpi");
    var itemCount = items.length;
    var total = 0;
      for(var i = 0; i < itemCount; i++)
        {
          total = total +  parseFloat(items[i].value);
        }
          $('#grand_total').val(total);
        }
</script>

@endsection








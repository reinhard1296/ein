@extends('layouts.app')
@section('content')
<div class="container">
	@if(\Session::has('error'))
	<div class="alert alert-danger">
	{{\Session::get('error')}}
</div>
@endif
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			@if(auth()->user()->isAdmin == 1)
			<div class="panel-heading">Welcome Admin {{ Auth::user()->wholename }}</div>
			<div class="panel-body">
				<h4>Administrator Functions</h4>
				-User Management <br>
				-Signatories <br>
				-Sole Distributor <br>
				-Purchase Request <br>
				-Request for Quotaiton <br>
				-Abstract <br>
				-Purchase Order <br>

			</div>
			@elseif(auth()->user()->isBACSec == 1)
			<div class="panel-heading">Welcome BAC Secretariat {{ Auth::user()->wholename }}</div>
			<div class="panel-body">
				<h4>BAC Secretariat Functions</h4>
				-Purchase Request <br>
				-Closing of Pending Purchase Requests <br>
				-Request for Quotaiton <br>
				-Abstract <br>
				-Purchase Order <br>
			</div>
			@else
			<div class="panel-heading">Welcome User {{ Auth::user()->name }}</div>
			<div class="panel-body">
				<h4>User Functions</h4>
				-Purchase Request <br>
				-Request for Quotaiton <br>
				-Abstract <br>
				-Purchase Order <br>
			</div>
			@endif
			
		</div>
	</div>
</div>
</div>
@endsection
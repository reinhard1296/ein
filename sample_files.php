<tbody>

                                @forelse($pr_table as $key => $items)
                                    <tr>
                                    <td>{{$items->id}}</td>           
                                    <td>{{$items->pr_qty}}</td>
                                    <td>{{$items->pr_unit}}</td>
                                    <td>{{$items->pr_description}}</td>
                                    <td>{{$items->pr_cost_per_unit}}</td>
                                    <td>{{$items->pr_estimated_cost}}</td>
                                    </tr>
                                @empty
                                    <tr >
                                        <td colspan="7">No Results</td>
                                    </tr>
                                @endforelse
                                
                            </tbody>


                            @section('script')

<script type="text/javascript">
    
    $(document).ready(function() {
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('pr.items.data') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'pr_qty', name: 'pr_qty'},
                {data: 'pr_unit', name: 'pr_unit'},
                {data: 'pr_description', name: 'pr_description'},
                {data: 'pr_cost_per_unit', name: 'pr_cost_per_unit'},
                {data: 'pr_estimated_cost', name: 'pr_estimated_cost'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
         

     
    });
    
  
        

</script>
@endsection




<script type="text/javascript">
    $(document).ready(function() {
  var i = 1;
  $("#add_item").click(function(grandTotal) {
  $('tr').find('input').prop('disabled',true)
    $('#addr' + i).html('<td class="custom-tbl"><input class="form-control input-sm"style="width:100%;" type="text" value="'+(i+1)+'" name="pr_item[]" readonly required></td><td class="custom-tbl"><input class="form-control input-sm" style="width:100%;" type="text" name="pr_qty[]"></td><td class="custom-tbl"><input class="form-control input-sm" style="width:100%;" type="text" name="pr_unit[]"></td><td class="custom-tbl"><input class="form-control input-sm" style="width:100%;" type="text" name="pr_desc[]"></td><td class="custom-tbl"><input class="form-control input-sm" style="width:100%;" type="text" name="pr_cpu[]"></td><td class="custom-tbl"><input class="estimated_cost form-control input-sm" style="width:100%;" type="text" name="pr_cpi[]"></td><td class="custom-tbl"><button id="add_item" type="button" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-plus"></span></button> <button id="remove_item" type="button" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></button></td>');

    $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
    i++;

    var items = document.getElementsByClassName("estimated_cost");
    var itemCount = items.length;
    var total = 0;
    for(var j = 0; i < itemCount; j++)
    {
            total = total +  parseFloat(items[i].value);
    }
        document.getElementById('grand_total').value 

    
  });
});
</script>